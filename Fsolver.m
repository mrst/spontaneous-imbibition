% we want to find the solution K(F) = 0

mrstModule add ad-core

n = 10; % number of segment
N = n + 1; % number of points (values of F)

% initial guess
F = ones(N, 1);

alpha = alphaFun(n);


% we solve using Newton

Fad = initVariablesADI(F);
K = Kfun(Fad, alpha, n);
err = max(abs(K.val));

while err > 1e-8
    
    dF = -K.jac{1}\K.val;
    F = F + dF;
    Fad = initVariablesADI(F);
    K = Kfun(Fad, alpha, n);
    err = max(abs(K.val));
    
end







