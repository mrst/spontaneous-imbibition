function H = Hfun(F, alpha, n)
    N = n + 1;
    for i = 1 : N
        for j = 1 : (i - 1)
            alpha(i, j) = 0;
        end
    end
    H = alpha*(1./F);
end