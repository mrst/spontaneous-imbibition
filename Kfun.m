function K = Kfun(F, alpha, n)
    H = Hfun(F, alpha, n);
    H0 = H(1);
    K = H0*F + H - H0;
end
