function alpha = alphaFun(n)
    N = n + 1;
    deltax = 1/n ;
    
    alpha = zeros(N);
    
    for i = 1 : N
        xi = deltax*(i - 1);
        for j = 1 : N
            xj = deltax*(j - 1);            
            alpha(i, j) = (xj - xi)*Dfun(xj)*deltax;
        end
    end
    
end

           